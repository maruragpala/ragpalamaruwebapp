from django.forms import ModelForm
from . import models

class RestaurantForm(ModelForm):
  class Meta:
    model = models.Restaurant
    exclude = ('user', 'date',)

class DishForm(ModelForm):
  class Meta:
    model = models.Dish
    exclude = ('user', 'date', 'restaurant',)
